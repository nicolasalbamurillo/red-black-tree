package org.fjala.redblacktree;

public class RedBlackTree<T, K extends Comparable<K>> {
    private static final RedBlackNode<?, ?> NIL = null;
    private RedBlackNode<T, K> root;

    public RedBlackTree(RedBlackNode<T, K> root) {
        this.root = root;
    }

    public void rotateLeft(RedBlackNode<T, K> x) {
        RedBlackNode<T, K> y = x.right;
        x.right = y.left;
        if (y.left != NIL) {
            y.left.parent = x;
        }
        y.parent = x.parent;
        if (x.parent == NIL) {
            root = y;
        } else if (x.parent.left == x) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }
        y.left = x;
        x.parent = y;
    }

    public void rotateRight(RedBlackNode<T, K> y) {
        RedBlackNode<T, K> x = y.left;
        y.left = x.right;
        if (x.right != NIL) {
            x.right.parent = y;
        }
        x.parent = y.parent;
        if (y.parent == NIL) {
            root = x;
        } else if (y.parent.left == y) {
            y.parent.left = x;
        } else {
            y.parent.right = x;
        }
        x.right = y;
        y.parent = x;
    }

    public RedBlackNode<T, K> getRoot() {
        return root;
    }

    public static RedBlackNode<?, ?> getNIL() {
        return NIL;
    }
}
