package org.fjala.redblacktree;

public class RedBlackNode <T, K extends Comparable<K>> {
    protected T value;
    protected K key;
    protected RedBlackNode<T, K> left;
    protected RedBlackNode<T, K> right;
    protected RedBlackNode<T, K> parent;
    protected boolean isRed;

    public RedBlackNode(T value, K key, RedBlackNode<T, K> parent) {
        this.value = value;
        this.key = key;
        this.parent = parent;
    }

    public RedBlackNode(T value, K key, RedBlackNode<T, K> left, RedBlackNode<T, K> right, RedBlackNode<T, K> parent) {
        this.value = value;
        this.key = key;
        this.left = left;
        this.right = right;
        this.left.parent = this;
        this.right.parent = this;
        this.parent = parent;
    }

    public static <T, K extends Comparable<K>> RedBlackNode<T, K> of(T value, K key) {
        return new RedBlackNode<>(value, key, null);
    }

    public static <T, K extends Comparable<K>> RedBlackNode<T, K> of(T value, K key, RedBlackNode<T, K> left, RedBlackNode<T, K> right) {
        return new RedBlackNode<>(value, key, left, right, null);
    }
}
