package org.fjala.redblacktree;

public class BinaryTree <T> {
    private final T root;
    private BinaryTree<T> left;
    private BinaryTree<T> right;

    public BinaryTree(T root) {
        this.root = root;
    }

    public BinaryTree(T root, BinaryTree<T> left, BinaryTree<T> right) {
        this.root = root;
        this.left = left;
        this.right = right;
    }

    public T getRoot() {
        return root;
    }

    public BinaryTree<T> getLeft() {
        return left;
    }

    public BinaryTree<T> getRight() {
        return right;
    }

    public void setLeft(BinaryTree<T> left) {
        this.left = left;
    }

    public void setRight(BinaryTree<T> right) {
        this.right = right;
    }

    public boolean isLeaf() {
        return left == null && right == null;
    }

    public static <T> BinaryTree<T> of(T root) {
        return new BinaryTree<>(root);
    }

    public static <T> BinaryTree<T> of(T root, BinaryTree<T> left, BinaryTree<T> right) {
        return new BinaryTree<>(root, left, right);
    }
}
