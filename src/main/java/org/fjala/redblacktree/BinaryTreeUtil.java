package org.fjala.redblacktree;

public class BinaryTreeUtil {

    public boolean isTreeApproximatelyBalanced(BinaryTree<?> tree) {
        if (tree == null) {
            return true;
        }
        return minPath(tree) * 2 >= maxPath(tree) && isTreeApproximatelyBalanced(tree.getLeft()) && isTreeApproximatelyBalanced(tree.getRight());
    }

    public int maxPath(BinaryTree<?> tree) {
        if (tree == null) {
            return 0;
        }
        if (tree.getRight() == null && tree.getLeft() == null) {
            return 1;
        }
        return Math.max(maxPath(tree.getRight()), maxPath(tree.getLeft())) + 1;
    }

    public int minPath(BinaryTree<?> tree) {
        if (tree == null) {
            return 0;
        }
        if (tree.getRight() == null && tree.getLeft() == null) {
            return 1;
        }
        return Math.min(maxPath(tree.getRight()), maxPath(tree.getLeft())) + 1;
    }
}
