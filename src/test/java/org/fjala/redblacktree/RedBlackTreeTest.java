package org.fjala.redblacktree;

import org.junit.jupiter.api.Test;

import static org.fjala.redblacktree.RedBlackNode.of;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RedBlackTreeTest {
    @Test
    void rotateLeftShouldRotateLeftTheTree() {
        RedBlackNode<String, String> root =
                of("X","X",
                        of("T1", "T1"),
                        of("Y", "Y",
                                of("T2", "T2"),
                                of("T3", "T3")));
        RedBlackTree<String, String> tree = new RedBlackTree<>(root);

        tree.rotateLeft(root);

        root = tree.getRoot();
        assertEquals("Y", root.value);
        assertEquals("X", root.left.value);
        assertEquals("T3", root.right.value);
        assertEquals("T1", root.left.left.value);
        assertEquals("T2", root.left.right.value);
    }

    @Test
    void leftAndRightRotateShouldKeepTheInitialStructure() {
        RedBlackNode<String, String> root =
                of("X","X",
                        of("T1", "T1"),
                        of("Y", "Y",
                                of("T2", "T2"),
                                of("T3", "T3")));

        RedBlackTree<String, String> tree = new RedBlackTree<>(root);

        tree.rotateLeft(tree.getRoot());
        tree.rotateRight(tree.getRoot());

        root = tree.getRoot();
        assertEquals("X", root.value);
        assertEquals("T1", root.left.value);
        assertEquals("Y", root.right.value);
        assertEquals("T2", root.right.left.value);
        assertEquals("T3", root.right.right.value);
    }
}
