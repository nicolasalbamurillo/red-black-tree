package org.fjala.redblacktree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.fjala.redblacktree.BinaryTree.of;

class BinaryTreeUtilTest {

    @Test
    void minPathShouldReturnTheMinPathOfATree() {
        BinaryTreeUtil util = new BinaryTreeUtil();
        BinaryTree<String> tree =
                of("B",
                        of("A"),
                        of("X",
                                of("C"),
                                of("Y",
                                        null,
                                        of("Z"))));

        int result = util.minPath(tree);

        assertEquals(2, result);
    }

    @Test
    void maxPathShouldReturnTheMaxPathOfATree() {
        BinaryTreeUtil util = new BinaryTreeUtil();
        BinaryTree<String> tree =
                of("B",
                        of("A"),
                        of("X",
                                of("C"),
                                of("Y",
                                        null,
                                        of("Z"))));

        int result = util.maxPath(tree);

        assertEquals(4, result);
    }

    @Test
    void isTreeApproximatelyBalancedShouldReturnTrueWhenTreeItIs() {
        BinaryTreeUtil util = new BinaryTreeUtil();
        BinaryTree<String> tree =
                of("B",
                        of("A"),
                        of("X",
                                of("C"),
                                of("Y",
                                        null,
                                        of("Z"))));

        assertTrue(util.isTreeApproximatelyBalanced(tree));
    }

    @Test
    void isTreeApproximatelyBalancedShouldReturnFalseWhenTreeItIsNot() {
        BinaryTreeUtil util = new BinaryTreeUtil();
        BinaryTree<String> tree =
                of("B",
                        null,
                        of("X",
                                of("C"),
                                of("Y",
                                        null,
                                        of("Z"))));

        assertFalse(util.isTreeApproximatelyBalanced(tree));
        assertEquals(4 ,util.maxPath(tree));
        assertEquals(1, util.minPath(tree));
    }
}